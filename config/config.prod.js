/* eslint valid-jsdoc: "off" */

'use strict';

/**
 * @param {Egg.EggAppInfo} appInfo app info
 */
module.exports = appInfo => {
  /**
   * built-in config
   * @type {Egg.EggAppConfig}
   **/
  const config = exports = {};

  // use for cookie sign key, should change to your own and keep security
  config.keys = appInfo.name + '_1586313704856_7796';

  // jwt配置
  config.jwt = {
    secret: 'JARVIS#2020',
  };

  // swagger配置
  config.swagger = {
    dirScanner: './app/controller', // 配置自动扫描的控制器路径
    basePath: 'https://api.worklog.qingxzd.com', // api前置路由
    apiInfo: {
      title: 'dailyLog', // 接口文档的标题
      description: 'swagger-ui for dailyLog document.', // 接口文档描述
      version: '1.0.0', // 接口文档版本
    },
    schemes: ['http', 'https'], // 配置支持的协议
    consumes: ['application/json'], // 指定处理请求的提交内容类型（Content-Type），例如application/json, text/html
    produces: ['application/json'], // 指定返回的内容类型，仅当request请求头中的(Accept)类型中包含该指定类型才返回
    securityDefinitions: {}, // 配置接口安全授权方式
  };

  // 配置mongose连接mongodb数据库
  config.mongoose = {
    client: {
      url: 'mongodb://work_log:work_log@mongo:27017/work_log',
      options: {
        useNewUrlParser: true,
        useUnifiedTopology: true,
      },
    },
  };

  // app info
  config.appConfig = {
    appkey: 'dingiigffpbxxkfqheny',
    appsecret: 'u7oukwfiO9WFOZ-qt-gCF_sKTG0OGp8A5jKkLNDbnFTWMdB4NO0-b3dKflBh5xth',
    agent_id: '537601165',
    corpId: 'ding57ff116cf0c88b8f35c2f4657eb6378f',
  };
  return {
    ...config,
  };
};
