'use strict';

/**
 * @param {Egg.EggAppInfo} appInfo app info
 */
module.exports = appInfo => {
  /**
   * built-in config
   * @type {Egg.EggAppConfig}
   **/
  const config = exports = {};

  // use for cookie sign key, should change to your own and keep security
  config.keys = appInfo.name + '_1586313704856_7796';

  // 关闭csrf验证
  config.security = {
    csrf: {
      enable: false,
    },
  };

  // jwt配置
  config.jwt = {
    secret: 'JARVIS#2020',
  };

  // swagger配置
  config.swagger = {
    dirScanner: './app/controller', // 配置自动扫描的控制器路径
    mountPath: ' /eapp',
    basePath: '/', // api前置路由
    apiInfo: {
      title: 'dailyLog', // 接口文档的标题
      description: 'swagger-ui for dailyLog document.', // 接口文档描述
      version: '1.0.0', // 接口文档版本
    },
    schemes: ['http', 'https'], // 配置支持的协议
    consumes: ['application/json'], // 指定处理请求的提交内容类型（Content-Type），例如application/json, text/html
    produces: ['application/json'], // 指定返回的内容类型，仅当request请求头中的(Accept)类型中包含该指定类型才返回
    securityDefinitions: {}, // 配置接口安全授权方式
  };

  // 跨域配置
  config.cors = {
    origin: '*', // 表示允许的源
    allowMethods: 'GET,HEAD,PUT,POST,DELETE,PATCH,OPTIONS', // 表示允许的http请求方式
  };

  // 配置mongose连接mongodb数据库
  // Mongodb://eggadmin:123456@localhost:27017 //有用户名密码的情况
  config.mongoose = {
    client: {
      url: 'mongodb://work_log:work_log@localhost:27017/work_log',
      options: {
        useNewUrlParser: true,
        useUnifiedTopology: true,
      },
    },
  };
  
  // app info
  config.appConfig = {
    appkey: 'dingiigffpbxxkfqheny',
    appsecret: 'u7oukwfiO9WFOZ-qt-gCF_sKTG0OGp8A5jKkLNDbnFTWMdB4NO0-b3dKflBh5xth',
    agent_id: '537601165',
    corpId: 'ding57ff116cf0c88b8f35c2f4657eb6378f',
  };

  return {
    ...config,
  };
};
