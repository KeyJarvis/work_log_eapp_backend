'use strict';

/** @type Egg.EggPlugin */
module.exports = {
  // 跨域插件
  cors: {
    enable: true,
    package: 'egg-cors',
  },
  // mmongo插件
  mongoose: {
    enable: true,
    package: 'egg-mongoose',
  },
  // egg-jwt插件
  jwt: {
    enable: true,
    package: 'egg-jwt',
  },
  // swagger-ui插件
  swaggerdoc: {
    enable: true,
    package: 'egg-swagger-doc',
  },
};
