'use strict';

const Controller = require('egg').Controller;
const moment = require('moment');
/**
 * @controller daily 工作日志接口
 */

class DailyController extends Controller {

  /**
   * @summary 创建日报
   * @description 创建日报
   * @router post /daily/create
   * @request header string Authorization 用户token
   * @request body postCreateRequest *body 请求体内容
   * @response 200 createResponse
   */
  async create() {
    const { ctx } = this;
    const data = ctx.request.body;
    const token = ctx.request.headers.authorization;
    const res = await ctx.service.daily.create(data, token);
    const msg = '添加成功';
    ctx.helper.success(ctx, msg, res);
  }

  /**
   * @summary 根据日期获取日报
   * @description 根据日期获取日报
   * @router get /daily/get
   * @request header String Authorization 用户token
   * @request query Number date 20200508
   * @response 200 getResponse
   *
   */
  async get() {
    const { ctx } = this;
    const date = ctx.query.date;
    const token = ctx.request.headers.authorization;
    const res = await ctx.service.daily.get(date, token);
    if (res.length === 0) {
      const msg = '今日未提交日报';
      ctx.helper.fail(ctx, msg);
    } else {
      const msg = '获取日报成功';
      ctx.helper.success(ctx, msg, res);
    }
  }

  /**
   * @summary 更新日报
   * @description 更新日报
   * @router put /daily/update
   * @request header String Authorization 用户token
   * @request body postUpdateRequest *body 请求体内容
   * @response 200 updateResponse
   */
  async update() {
    const { ctx } = this;
    const data = ctx.request.body;
    const res = await ctx.service.daily.update(data);
    if (!res) {
      const msg = '更新日报失败';
      ctx.helper.fail(ctx, msg);
    } else {
      const msg = '更新日报成功';
      ctx.helper.success(ctx, msg);
    }
  }

  /**
  * @summary 获取日报统计数据
  * @description 获取日报统计数据
  * @router get /daily/statistic
  * @request header String Authorization 用户token
  * @response 200 statisticResponse
  */
  async statistic() {
    const { ctx } = this;
    let date = '';
    if (new Date().getHours() < 10) {
      date = moment().subtract(1, 'days').format('YYYYMMDD');
    } else {
      date = moment().format('YYYYMMDD');
    }
    const res = await ctx.service.daily.statistic(date);
    const msg = '获取日报统计数据成功';
    ctx.helper.success(ctx, msg, res);
  }

  /**
     * @summary 获取未交日期
     * @description 获取未交日期
     * @router get /daily/lack
     * @request header String Authorization 用户token
     * @response 200 lackResponse
     */
  async lack() {
    const { ctx } = this;
    const token = ctx.request.headers.authorization;
    const res = await ctx.service.daily.lack(token);
    const msg = '获取未交日期成功';
    ctx.helper.success(ctx, msg, res);
  }

  /**
   * @summary 获取导出范围
   * @description 获取导出范围
   * @router get /daily/range
   * @request header String Authorization 用户token
   * @response 200 lackResponse
   */
  async range() {
    const { ctx } = this;
    const token = ctx.request.headers.authorization;
    const res = await ctx.service.daily.range(token);
    const msg = '获取导出范围成功';
    ctx.helper.success(ctx, msg, res);
  }

  /**
   * @summary 导出日报
   * @description 导出日报
   * @router post /daily/export
   * @request header String Authorization 用户token
   * @request body postExportRequest *body 请求体内容
   * @response 200 exportResponse
   */
  async export() {
    const { ctx } = this;
    const userInfo = ctx.state.user;
    const params = ctx.request.body;
    const res = await ctx.service.daily.export(userInfo, params);
    if (res) {
      const msg = '导出日报成功';
      ctx.helper.success(ctx, msg, res);
    } else {
      const msg = '导出日报失败,没有有效数据！';
      ctx.helper.fail(ctx, msg);
    }
  }
}

module.exports = DailyController;
