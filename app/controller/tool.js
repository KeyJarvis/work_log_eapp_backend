'use strict';

const Controller = require('egg').Controller;

/**
 * @controller tool 工具类接口
 */

class ToolController extends Controller {

  /**
  * @summary 根据前端code获取用户信息
  * @description 根据code查询用户信息
  * @router get /login
  * @request query Number code code
  * @response 200 loginResponse
  */
  async login() {
    const { ctx, app } = this;
    const code = ctx.query.code;
    const res = await ctx.service.tool.login(code);
    const msg = '钉钉免登成功!';
    const token = app.jwt.sign({ userid: res.userid, identity: res.identity }, app.config.jwt.secret);
    const data = { userInfo: res, token };
    ctx.helper.success(ctx, msg, data);
  }

  /**
  * @summary 计算签名信息返回签名
  * @description 计算签名信息返回签名
  * @router get /jsapi_sign
  * @response 200 signResponse
  */
  async sign() {
    const { ctx } = this;
    const res = await ctx.service.tool.sign();
    const msg = '获取签名成功';
    ctx.helper.success(ctx, msg, res);
  }
  /**
      * @summary 获取所有用户信息
      * @description 获取所有用户信息
      * @router get /tool/users
      * @request header string Authorization 用户token
      * @response 200 getAllUserInfo
      */
  async getAllUserInfo() {
    const { ctx, service } = this;
    ctx.body = await service.tool.getAllUserInfo();
  }

  /**
      * @summary 获取所有部门信息
      * @description 获取所有部门信息
      * @router get /tool/departments
      * @request header string Authorization 用户token
      * @response 200 getAllDepartments
      */
  async getAllDepartments() {
    const { ctx, service } = this;
    ctx.body = await service.tool.getAllDepartments();
  }

  /**
       * @summary 更新用户和部门信息
       * @description 更新用户和部门信息
       * @router get /tool/update
       * @request header string Authorization 用户token
       * @response 200 updateData
       */
  async updateData() {
    const { ctx, service } = this;
    ctx.body = await service.tool.updateData();
    const msg = '更新钉钉数据成功';
    ctx.helper.success(ctx, msg);
  }

}

module.exports = ToolController;
