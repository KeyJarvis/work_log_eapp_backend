'use strict';

const Controller = require('egg').Controller;
/**
 * @controller system 系统设置
 */
class SystemController extends Controller {

  /**
   * @summary 创建管理员
   * @description 创建管理员
   * @router post /system/createAdmin
   * @request header string Authorization 用户token
   * @request body postCreateAdmin *body 请求体内容
   * @response 200 createAdmin
   */
  async createAdmin() {
    const { ctx } = this;
    const data = ctx.request.body;
    const res = await ctx.service.system.createAdmin(data);
    const msg = '添加管理员成功';
    ctx.helper.success(ctx, msg);
  }

  /**
   * @summary 删除管理员
   * @description 删除管理员
   * @router get /system/deleteAdmin
   * @request header string Authorization 用户token
   * @request query String name 管理员名字
   * @response 200 deleteAdmin
   */
  async deleteAdmin() {
    const { ctx } = this;
    const name = ctx.query.name;
    const res = await ctx.service.system.deleteAdmin(name);
    const msg = '删除管理员成功';
    ctx.helper.success(ctx, msg);
  }

  /**
  * @summary 获取管理员列表
  * @description 获取管理员列表
  * @router get /system/adminList
  * @request header string Authorization 用户token
  * @response 200 adminList
  */
  async adminList() {
    const { ctx } = this;
    const res = await ctx.service.system.adminList();
    const msg = '获取管理员列表成功';
    ctx.helper.success(ctx, msg, res);
  }

  /**
   * @summary 添加白名单
   * @description 添加白名单
   * @router post /system/createWhite
   * @request header string Authorization 用户token
   * @request body postCreateWhite *body 请求体内容
   * @response 200 createWhite
   */
  async createWhite() {
    const { ctx } = this;
    const data = ctx.request.body;
    const res = await ctx.service.system.createWhite(data);
    const msg = '添加白名单成功';
    ctx.helper.success(ctx, msg, res);
  }

  /**
  * @summary 删除白名单
  * @description 删除白名单
  * @router get /system/deleteWhite
  * @request header string Authorization 用户token
  * @response 200 deleteWhite
  */
  async deleteWhite() {
    const { ctx } = this;
    const userid = ctx.query.userid;
    const res = await ctx.service.system.deleteWhite(userid);
    const msg = '删除白名单成功';
    ctx.helper.success(ctx, msg, res);
  }

  /**
  * @summary 获取白名单列表
  * @description 获取白名单列表
  * @router get /system/whiteList
  * @request header string Authorization 用户token
  * @response 200 whiteList
  */
  async whiteList() {
    const { ctx } = this;
    const res = await ctx.service.system.whiteList();
    const msg = '获取白名单列表成功';
    ctx.helper.success(ctx, msg, res);
  }
}
module.exports = SystemController;
