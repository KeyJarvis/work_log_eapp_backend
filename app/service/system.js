'use strict';

const Service = require('egg').Service;
const moment = require('moment');
class SystemService extends Service {

  // 添加管理员
  async createAdmin(data) {
    const res = await this.ctx.model.Users.updateMany({ name: { $in: data.name } }, { identity: data.identity, identityTime: moment().format('YYYY-MM-DD') });
    return res;
  }

  // 删除管理员
  async deleteAdmin(name) {
    await this.ctx.model.Users.updateOne({ name }, { identity: 'ordinary', identityTime: '' });
    return [];
  }

  // 获取管理员列表
  async adminList() {
    const res = await this.ctx.model.Users.find({ identity: { $in: [ 'root', 'admin' ] } });
    for (let i = 0; i < res.length; i++) {
      if (res[i].identity === 'root') {
        res[i].identity = '超级管理员';
      } else {
        res[i].identity = '子管理员';
      }
      const result = await this.ctx.model.Departments.find({ id: { $in: res[i].department } }, 'name');
      const nameList = result.map(item => item.name).toString();
      res[i].department = nameList;
    }
    return res;
  }

  // 添加白名单
  async createWhite(data) {
    const res = await this.ctx.model.Settings.find({ name: '白名单' });
    if (res.length === 0) {
      await this.ctx.model.Settings.create({ whiteList: data.userid });
    } else {
      await this.ctx.model.Settings.updateMany({ name: '白名单' }, { $addToSet: { whiteList: data.userid } });
    }
    return res;
  }

  // 删除白名单
  async deleteWhite(userid) {
    await this.ctx.model.Settings.updateOne({ name: '白名单' }, { $pull: { whiteList: userid } });
    return [];
  }

  // 获取白名单列表
  async whiteList() {
    const res = await this.ctx.model.Settings.find();
    const whiteList = [];
    if (res.length !== 0) {
      for (let i = 0; i < res[0].whiteList.length; i++) {
        const result = await this.ctx.model.Users.find({ userid: { $in: res[0].whiteList[i] } });
        const department = await this.ctx.model.Departments.find({ id: { $in: result[0].department } });
        const nameList = department.map(item => item.name).toString();
        whiteList[i] = { name: result[0].name, userid: result[0].userid, department: nameList };
      }
      return whiteList;
    } return [];
  }
}

module.exports = SystemService;
