'use strict';

const Service = require('egg').Service;
const superagent = require('superagent');
const crypto = require('crypto-js');
const cryptoRandomString = require('crypto-random-string');
class ToolService extends Service {

  // 获取用户信息（钉钉免登）
  async login(code) {
    const access_token = await this.getAccessToken();
    const res = await this.ctx.curl('https://oapi.dingtalk.com/user/getuserinfo', {
      method: 'GET',
      data: {
        code,
        access_token,
      },
      dataType: 'json',
    });
    // const userInfo = await this.ctx.curl('https://oapi.dingtalk.com/user/get', {
    //   method: 'GET',
    //   data: {
    //     userid: res.data.userid,
    //     access_token,
    //   },
    //   dataType: 'json',
    // });
    const userInfo = await this.ctx.model.Users.findOne({ userid: res.data.userid });
    // const isExist = await this.ctx.model.Users.findOne({ userid: userInfo.data.userid });
    // if (!isExist && res.data.errcode === 0) {
    //   await this.ctx.model.Users.create(userInfo.data);
    // }
    console.log('Login in' + userInfo);
    return userInfo;
  }

  // JSAPI获取签名
  async sign() {
    const access_token = await this.getAccessToken();
    const res = await this.ctx.curl('https://oapi.dingtalk.com/get_jsapi_ticket', {
      method: 'GET',
      data: {
        access_token,
      },
      dataType: 'json',
    });
    const nonceStr = cryptoRandomString({ length: 10, type: 'base64' });
    const timeStamp = new Date().getTime().toString();
    const url = 'https://api.worklog.qingxzd.com';
    const plain = 'jsapi_ticket=' + res.data.ticket + '&noncestr=' + nonceStr + '&timestamp=' + timeStamp + '&url=' + url;
    const signature = crypto.SHA1(plain).toString();
    const agentId = this.config.appConfig.agent_id;
    const corpId = this.config.appConfig.corpId;
    return { url, nonceStr, agentId, timeStamp, corpId, signature };
  }

  /**
    * @description 获取钉钉服务端API access_token
    * @return {string} access_token
    * @memberof ToolService
    */
  async getAccessToken() {
    const res = await this.ctx.curl('https://oapi.dingtalk.com/gettoken', {
      method: 'GET',
      data: {
        appkey: this.config.appConfig.appkey,
        appsecret: this.config.appConfig.appsecret,
      },
      dataType: 'json',
    });
    return res.data.access_token;
  }

  /**
   * @description 获取公司全部钉钉用户信息
   * @return {object[]} userlist
   * @memberof ToolService
   */
  async getAllUserInfo() {
    // 依次获取所有部门用户信息
    const departments = await this.getAllDepartments();
    let allUserInfo = [];
    if (departments) {
      for (const department of departments) {
        const res = await this.getDepartmentUsers(department.id);
        allUserInfo = allUserInfo.concat(res);
      }
    }
    // 数据去重
    const hash = {};
    allUserInfo = allUserInfo.filter(d => d);
    allUserInfo = allUserInfo.reduce((item, next) => {
      hash[next.userid] ? '' : (hash[next.userid] = true && item.push(next));
      return item;
    }, []);

    // 筛选指定字段
    const newData = allUserInfo.map(item => {
      return {
        unionid: item.unionid,
        userid: item.userid,
        department: item.department,
        name: item.name,
        avatar: item.avatar,
      };
    });
    return newData;
  }

  /**
  * @description 获取所有部门基础信息
  * @return {object[]} 部门信息列表
  * @memberof ToolService
  */
  async getAllDepartments() {
    const access_token = await this.getAccessToken();
    if (access_token) {
      try {
        const res = await this.ctx.curl('https://oapi.dingtalk.com/department/list', {
          method: 'GET',
          data: {
            access_token,
          },
          dataType: 'json',
        });
        if (res.data.errcode === 0) {
          return res.data.department.map(item => {
            return {
              name: item.name,
              id: item.id,
              parentid: item.parentid,
            };
          });
        }
        return false;
      } catch (error) {
        console.error(error);
        return false;
      }
    }
  }

  /**
   * @description 根据部门id获取部门用户信息
   * @param {*} department_id 部门id
   * @return {object[]} userlist
   * @memberof ToolService
   */
  async getDepartmentUsers(department_id) {
    const access_token = await this.getAccessToken();
    const res = await this.ctx.curl('https://oapi.dingtalk.com/user/listbypage', {
      method: 'GET',
      data: {
        access_token,
        department_id,
        offset: '0',
        size: 100,
      },
      dataType: 'json',
    });
    return res.data.userlist;
  }

  /**
   * @description 更新钉钉用户信息及部门信息
   * @return {Boolean} 成功
   * @memberof ToolService
   */
  async updateData() {
    // 更新用户表
    const users = await this.getAllUserInfo();
    if (users.length !== 0) {
      const useridList = users.map(item => item.userid);
      // 删除不存在用户
      await this.ctx.model.Users.deleteMany({ userid: { $nin: useridList } });
      // 更新用户信息
      for (let i = 0; i < users.length; i++) {
        const res = await this.ctx.model.Users.updateOne({ userid: users[i].userid }, users[i]);
        // 更新失败则创建该用户
        if (res.n === 0) {
          const res = await this.ctx.model.Users.create(users[i]);
          console.log('新增用户信息' + res);
        }
      }
    }
    // 更新部门表
    const departments = await this.getAllDepartments();
    if (departments) {
      const idList = departments.map(item => item.id);
      // 删除不存在部门
      await this.ctx.model.Departments.deleteMany({ id: { $nin: idList } });
      // 更新部门信息
      for (let i = 0; i < departments.length; i++) {
        const res = await this.ctx.model.Departments.update({ id: departments[i].id }, departments[i]);
        // 更新失败则创建该部门
        if (res.n === 0) {
          const res = await this.ctx.model.Departments.create(departments[i]);
          console.log('新增部门信息' + res);
        }
      }
    }
    return true;
  }

  // 上传文件
  async uploadMedia(filePath) {
    const access_token = await this.getAccessToken();
    const url = 'https://oapi.dingtalk.com/media/upload';
    const uploadRes = await superagent
      .post(url)
      .query({
        access_token,
        type: 'file',
      })
      .attach('media', filePath);
    console.log('Dingtalk upload media: ', filePath, 'Res: ', uploadRes.body);
    return uploadRes.body;
  }

  // 发送导出的excel文件
  async sendMsg(media_id, userid_list) {
    const access_token = await this.getAccessToken();
    const url = 'https://oapi.dingtalk.com/topapi/message/corpconversation/asyncsend_v2';
    const query = {
      access_token,
    };
    console.log('user_list: ', userid_list);
    const payload = {
      agent_id: this.config.appConfig.agent_id,
      toAllUser: false,
      userid_list,
      msg: {
        msgtype: 'file',
        file: {
          media_id,
        },
      },
    };
    const sendMsgRes = await superagent
      .post(url)
      .query(query)
      .send(payload);
    console.log('sendMsgRes: ', sendMsgRes.body);
    return sendMsgRes.body;
  }

  // 发送文本提示通知
  async sendTextMsg(content, userid_list) {
    const access_token = await this.getAccessToken();
    const url = 'https://oapi.dingtalk.com/topapi/message/corpconversation/asyncsend_v2';
    const query = {
      access_token,
    };
    console.log('user_list: ', userid_list);
    const payload = {
      agent_id: this.config.appConfig.agent_id,
      toAllUser: false,
      userid_list,
      msg: {
        msgtype: 'text',
        text: {
          content,
        },
      },
    };
    const sendMsgRes = await superagent
      .post(url)
      .query(query)
      .send(payload);
    console.log('sendMsgRes: ', sendMsgRes.body);
    return sendMsgRes.body;
  }
}

module.exports = ToolService;
