/* eslint-disable array-bracket-spacing */
'use strict';

const XLSX = require('xlsx');
const moment = require('moment');
const Service = require('egg').Service;
class DailyService extends Service {

  // 根据日期获取日报
  async get(date, token) {
    const { ctx } = this;
    const userInfo = await this.app.jwt.verify(token.split(' ')[1], this.app.config.jwt.secret); // token解析
    const result = await ctx.model.DailyLogs.find({ date, userid: userInfo.userid }).exec();
    return result;
  }

  // 提交日报
  async create(data, token) {
    const userInfo = await this.app.jwt.verify(token.split(' ')[1], this.app.config.jwt.secret); // token解析
    const findRecord = await this.ctx.model.DailyLogs.findOne({
      date: data.date,
      userid: userInfo.userid,
      isDelete: false,
    }).exec();
    if (!findRecord) {
      const id = await this.ctx.model.Users.findOne({ userid: userInfo.userid });
      data.name = id._id;
      data.userid = userInfo.userid;
      const newRecord = await this.ctx.model.DailyLogs.create(data);
      return { id: newRecord._id }; // 新增日报数据
    }
    const updateRecord = await this.ctx.model.DailyLogs.updateOne(
      {
        date: data.date,
        userid: userInfo.userid,
        isDelete: false,
      },
      data
    ).exec();
    return { id: updateRecord._id }; // 更新日报数据
  }

  // 根据日报id更新日报
  async update(data) {
    await this.ctx.model.DailyLogs.updateOne(
      {
        _id: data.id,
        isDelete: false,
      },
      data
    ).exec();
    return { id: '日报id' }; // 更新日报数据
  }

  // 获取日报统计数据
  async statistic(date) {
    // 已提交日报用户
    const commitRes = await this.ctx.model.DailyLogs.find({
      date,
      workMatters: { $elemMatch: { $ne: null } },
    }).populate('name', 'name');

    console.log(commitRes);
    // 未交日报用户
    const commitUserid = commitRes.map(item => item.userid);
    const uncommitRes = await this.ctx.model.Users.find({
      userid: { $nin: commitUserid },
    });
    const successList = commitRes.map(item => {
      return { name: item.name.name, status: 'commit' };
    });
    const uppaidList = uncommitRes.map(item => {
      return { name: item.name, status: 'uncommit' };
    });
    const arr = successList.concat(uppaidList);
    const res = await this.ctx.model.Settings.find();
    let whiteList = [];
    if (res.length !== 0) {
      whiteList = await this.ctx.model.Users.find({ userid: { $in: res[0].whiteList } }, 'name');
    }
    let statisticsList = arr;
    for (let i = 0; i < whiteList.length; i++) {
      statisticsList = statisticsList.filter(item => { return item.name !== whiteList[i].name; });
    }
    const result = {
      commit: commitRes.length,
      uncommit: statisticsList.length - commitRes.length,
      statisticsList,
    };
    return result;
  }

  // 获取未交日期
  async lack(token) {
    const userInfo = await this.app.jwt.verify(token.split(' ')[1], this.app.config.jwt.secret); // token解析
    const res = await this.ctx.model.DailyLogs.find({
      userid: userInfo.userid,
      workMatters: { $elemMatch: { $ne: null } },
    }, 'date');
    let alreadyDateList = [];
    if (res.length !== 0) {
      alreadyDateList = res.map(item => item.date);
    }
    return { alreadyDateList };
  }
  // 获取导出范围
  async range(token) {
    const userInfo = await this.app.jwt.verify(token.split(' ')[1], this.app.config.jwt.secret);
    if (userInfo.identity === 'admin') {
      const res = await this.ctx.model.Users.findOne({ userid: userInfo.userid }, 'department');
      const result = await this.ctx.model.Departments.findOne({ id: res.department[0] }, 'name');
      return [{ value: 0, label: '自己' }, { value: res.department[0], label: result.name }];
    } else if (userInfo.identity === 'root') {
      const res = await this.ctx.model.Users.findOne({ userid: userInfo.userid }, 'department');
      const result = await this.ctx.model.Departments.findOne({ id: res.department[0] }, 'name');
      return [{ value: 0, label: '自己' }, { value: res.department[0], label: result.name }, { value: -1, label: '所有人' }];
    } return [{ value: 0, label: '自己' }];
  }

  // 导出日报数据
  async export(userInfo, params) {
    const date = moment().format('YYYYMMDD');
    if (params.type === 0) {
      // 针对自己导出
      switch (params.time) {
        // 昨天
        case 0: {
          const findDailyLogRes = await this.ctx.model.DailyLogs.find({
            date: date - 1,
            userid: userInfo.userid,
            isDelete: false,
            workMatters: { $elemMatch: { $ne: null } },
          }).exec();
          // 数据为空返回
          if (findDailyLogRes.length === 0) {
            return false;
          }
          // 发送工作通知
          await this._sendDailyLogMsg(findDailyLogRes, '昨天', userInfo.userid);
          return true;
        }
        // 今天
        case 1: {
          const findDailyLogRes = await this.ctx.model.DailyLogs.find({
            date,
            userid: userInfo.userid,
            isDelete: false,
            workMatters: { $elemMatch: { $ne: null } },
          }).exec();
          // 数据为空返回
          if (findDailyLogRes.length === 0) {
            return false;
          }
          // 发送工作通知
          await this._sendDailyLogMsg(findDailyLogRes, '今天', userInfo.userid);
          return true;
        }
        // 上周
        case 2: {
          const num = new Date().getDay();
          const endTime = moment().subtract(num, 'days').format('YYYYMMDD');
          const startTime = moment().subtract((num + 6), 'days').format('YYYYMMDD');
          const findDailyLogRes = await this.ctx.model.DailyLogs.find({
            date: {
              $gte: startTime,
              $lte: endTime,
            },
            userid: userInfo.userid,
            isDelete: false,
            workMatters: { $elemMatch: { $ne: null } },
          }).exec();
          // 数据为空返回
          if (findDailyLogRes.length === 0) {
            return false;
          }
          // 发送工作通知
          await this._sendDailyLogMsg(findDailyLogRes, '上周', userInfo.userid);
          return true;
        }
        // 本周
        case 3: {
          const num = new Date().getDay();
          const startTime = moment().subtract(num - 1, 'days').format('YYYYMMDD');
          const findDailyLogRes = await this.ctx.model.DailyLogs.find({
            date: {
              $gte: startTime,
              $lte: date,
            },
            userid: userInfo.userid,
            isDelete: false,
            workMatters: { $elemMatch: { $ne: null } },
          }).exec();
          // 数据为空返回
          if (findDailyLogRes.length === 0) {
            return false;
          }
          // 发送工作通知
          await this._sendDailyLogMsg(findDailyLogRes, '本周', userInfo.userid);
          return true;
        }
        // 上月
        case 4: {
          const time = moment().subtract(1, 'months').format('YYYYMM');
          const startTime = time + '01';
          const endtime = time + '31';
          const findDailyLogRes = await this.ctx.model.DailyLogs.find({
            date: {
              $gte: startTime,
              $lte: endtime,
            },
            userid: userInfo.userid,
            isDelete: false,
            workMatters: { $elemMatch: { $ne: null } },
          }).exec();
          // 数据为空返回
          if (findDailyLogRes.length === 0) {
            return false;
          }
          // 发送工作通知
          await this._sendDailyLogMsg(findDailyLogRes, '上月', userInfo.userid);
          return true;
        }
        // 本月
        case 5: {
          const time = moment().format('YYYYMM');
          const startTime = time + '01';
          const findDailyLogRes = await this.ctx.model.DailyLogs.find({
            date: {
              $gte: startTime,
              $lte: date,
            },
            userid: userInfo.userid,
            isDelete: false,
            workMatters: { $elemMatch: { $ne: null } },
          }).exec();
          // 数据为空返回
          if (findDailyLogRes.length === 0) {
            return false;
          }
          // 发送工作通知
          await this._sendDailyLogMsg(findDailyLogRes, '本月', userInfo.userid);
          return true;
        }
        // 自定义时间范围
        case 6: {
          const findDailyLogRes = await this.ctx.model.DailyLogs.find({
            date: {
              $gte: params.startTime,
              $lte: params.endTime,
            },
            userid: userInfo.userid,
            isDelete: false,
            workMatters: { $elemMatch: { $ne: null } },
          }).exec();
          // 数据为空返回
          if (findDailyLogRes.length === 0) {
            return false;
          }
          // 发送工作通知
          await this._sendDailyLogMsg(findDailyLogRes, '自定义', userInfo.userid);
          return true;
        }
        default:
          return false;
      }
    } else if (params.type === -1) {
      // 针对所有人导出
      switch (params.time) {
        // 昨天
        case 0: {
          const findDailyLogRes = await this.ctx.model.DailyLogs.find({
            date: date - 1,
            isDelete: false,
            workMatters: { $elemMatch: { $ne: null } },
          }).exec();
          console.log(findDailyLogRes);
          // 数据为空返回
          if (findDailyLogRes.length === 0) {
            return false;
          }
          // 发送工作通知
          await this._sendDailyLogMsg(findDailyLogRes, '昨天', userInfo.userid);
          return true;
        }
        // 今天
        case 1: {
          const findDailyLogRes = await this.ctx.model.DailyLogs.find({
            date,
            isDelete: false,
            workMatters: { $elemMatch: { $ne: null } },
          }).exec();
          // 数据为空返回
          if (findDailyLogRes.length === 0) {
            return false;
          }
          // 发送工作通知
          await this._sendDailyLogMsg(findDailyLogRes, '今天', userInfo.userid);
          return true;
        }
        // 上周
        case 2: {
          const num = new Date().getDay();
          const endTime = moment().subtract(num, 'days').format('YYYYMMDD');
          const startTime = moment().subtract((num + 6), 'days').format('YYYYMMDD');
          const findDailyLogRes = await this.ctx.model.DailyLogs.find({
            date: {
              $gte: startTime,
              $lte: endTime,
            },
            isDelete: false,
            workMatters: { $elemMatch: { $ne: null } },
          }).exec();
          // 数据为空返回
          if (findDailyLogRes.length === 0) {
            return false;
          }
          // 发送工作通知
          await this._sendDailyLogMsg(findDailyLogRes, '上周', userInfo.userid);
          return true;
        }
        // 本周
        case 3: {
          const num = new Date().getDay();
          const startTime = moment().subtract(num - 1, 'days').format('YYYYMMDD');
          const findDailyLogRes = await this.ctx.model.DailyLogs.find({
            date: {
              $gte: startTime,
              $lte: date,
            },
            isDelete: false,
            workMatters: { $elemMatch: { $ne: null } },
          }).exec();
          // 数据为空返回
          if (findDailyLogRes.length === 0) {
            return false;
          }
          // 发送工作通知
          await this._sendDailyLogMsg(findDailyLogRes, '本周', userInfo.userid);
          return true;
        }
        // 上月
        case 4: {
          const time = moment().subtract(1, 'months').format('YYYYMM');
          const startTime = time + '01';
          const endtime = time + '31';
          const findDailyLogRes = await this.ctx.model.DailyLogs.find({
            date: {
              $gte: startTime,
              $lte: endtime,
            },
            isDelete: false,
            workMatters: { $elemMatch: { $ne: null } },
          }).exec();
          // 数据为空返回
          if (findDailyLogRes.length === 0) {
            return false;
          }
          // 发送工作通知
          await this._sendDailyLogMsg(findDailyLogRes, '上月', userInfo.userid);
          return true;
        }
        // 本月
        case 5: {
          const time = moment().format('YYYYMM');
          const startTime = time + '01';
          const findDailyLogRes = await this.ctx.model.DailyLogs.find({
            date: {
              $gte: startTime,
              $lte: date,
            },
            isDelete: false,
            workMatters: { $elemMatch: { $ne: null } },
          }).exec();
          // 数据为空返回
          if (findDailyLogRes.length === 0) {
            return false;
          }
          // 发送工作通知
          await this._sendDailyLogMsg(findDailyLogRes, '本月', userInfo.userid);
          return true;
        }
        // 自定义时间范围
        case 6: {
          const findDailyLogRes = await this.ctx.model.DailyLogs.find({
            date: {
              $gte: params.startTime,
              $lte: params.endTime,
            },
            isDelete: false,
            workMatters: { $elemMatch: { $ne: null } },
          }).exec();
          // 数据为空返回
          if (findDailyLogRes.length === 0) {
            return false;
          }
          // 发送工作通知
          await this._sendDailyLogMsg(findDailyLogRes, '自定义', userInfo.userid);
          return true;
        }
        default:
          return false;
      }
    } else {
      // 针对子管理员导出部门
      const res = await this.ctx.model.Users.find({ department: params.type });
      const useridList = res.map(item => item.userid);
      switch (params.time) {
        // 昨天
        case 0: {
          const findDailyLogRes = await this.ctx.model.DailyLogs.find({
            date: date - 1,
            userid: { $in: useridList },
            isDelete: false,
            workMatters: { $elemMatch: { $ne: null } },
          }).exec();
          console.log(findDailyLogRes);
          // 数据为空返回
          if (findDailyLogRes.length === 0) {
            return false;
          }
          // 发送工作通知
          await this._sendDailyLogMsg(findDailyLogRes, '昨天', userInfo.userid);
          return true;
        }
        // 今天
        case 1: {
          const findDailyLogRes = await this.ctx.model.DailyLogs.find({
            date,
            userid: { $in: useridList },
            isDelete: false,
            workMatters: { $elemMatch: { $ne: null } },
          }).exec();
          // 数据为空返回
          if (findDailyLogRes.length === 0) {
            return false;
          }
          // 发送工作通知
          await this._sendDailyLogMsg(findDailyLogRes, '今天', userInfo.userid);
          return true;
        }
        // 上周
        case 2: {
          const num = new Date().getDay();
          const endTime = moment().subtract(num, 'days').format('YYYYMMDD');
          const startTime = moment().subtract((num + 6), 'days').format('YYYYMMDD');
          const findDailyLogRes = await this.ctx.model.DailyLogs.find({
            date: {
              $gte: startTime,
              $lte: endTime,
            },
            userid: { $in: useridList },
            isDelete: false,
            workMatters: { $elemMatch: { $ne: null } },
          }).exec();
          // 数据为空返回
          if (findDailyLogRes.length === 0) {
            return false;
          }
          // 发送工作通知
          await this._sendDailyLogMsg(findDailyLogRes, '上周', userInfo.userid);
          return true;
        }
        // 本周
        case 3: {
          const num = new Date().getDay();
          const startTime = moment().subtract(num - 1, 'days').format('YYYYMMDD');
          const findDailyLogRes = await this.ctx.model.DailyLogs.find({
            date: {
              $gte: startTime,
              $lte: date,
            },
            userid: { $in: useridList },
            isDelete: false,
            workMatters: { $elemMatch: { $ne: null } },
          }).exec();
          // 数据为空返回
          if (findDailyLogRes.length === 0) {
            return false;
          }
          // 发送工作通知
          await this._sendDailyLogMsg(findDailyLogRes, '本周', userInfo.userid);
          return true;
        }
        // 上月
        case 4: {
          const time = moment().subtract(1, 'months').format('YYYYMM');
          const startTime = time + '01';
          const endtime = time + '31';
          const findDailyLogRes = await this.ctx.model.DailyLogs.find({
            date: {
              $gte: startTime,
              $lte: endtime,
            },
            userid: { $in: useridList },
            isDelete: false,
            workMatters: { $elemMatch: { $ne: null } },
          }).exec();
          // 数据为空返回
          if (findDailyLogRes.length === 0) {
            return false;
          }
          // 发送工作通知
          await this._sendDailyLogMsg(findDailyLogRes, '上月', userInfo.userid);
          return true;
        }
        // 本月
        case 5: {
          const time = moment().format('YYYYMM');
          const startTime = time + '01';
          const findDailyLogRes = await this.ctx.model.DailyLogs.find({
            date: {
              $gte: startTime,
              $lte: date,
            },
            userid: { $in: useridList },
            isDelete: false,
            workMatters: { $elemMatch: { $ne: null } },
          }).exec();
          // 数据为空返回
          if (findDailyLogRes.length === 0) {
            return false;
          }
          // 发送工作通知
          await this._sendDailyLogMsg(findDailyLogRes, '本月', userInfo.userid);
          return true;
        }
        // 自定义时间范围
        case 6: {
          const findDailyLogRes = await this.ctx.model.DailyLogs.find({
            date: {
              $gte: params.startTime,
              $lte: params.endTime,
            },
            userid: { $in: useridList },
            isDelete: false,
            workMatters: { $elemMatch: { $ne: null } },
          }).exec();
          // 数据为空返回
          if (findDailyLogRes.length === 0) {
            return false;
          }
          // 发送工作通知
          await this._sendDailyLogMsg(findDailyLogRes, '自定义', userInfo.userid);
          return true;
        }
        default:
          return false;
      }
    }
  }

  // 发送日报数据工作通知
  async _sendDailyLogMsg(dailyLogData, type, userid) {
    const workArr = [];
    const workSchema = [
      {
        key: 'name',
        name: '姓名',
      },
      {
        key: 'department',
        name: '部门',
      },
      {
        key: 'date',
        name: '日期',
      },
      {
        key: 'matter',
        name: '工作事项',
      },
      {
        key: 'content',
        name: '工作内容',
      },
      {
        key: 'coworkers',
        name: '参与同事',
      },
      {
        key: 'partners',
        name: '参与伙伴',
      },
      {
        key: 'status',
        name: '状态效果',
      },
      {
        key: 'time',
        name: '所用时段',
      },
      {
        key: 'plan',
        name: '后续计划',
      },
      {
        key: 'suggestion',
        name: '问题建议',
      },
      {
        key: 'isLate',
        name: '是否补交',
      },
      {
        key: 'lateReason',
        name: '补交原因',
      },
      {
        key: 'info',
        name: '是否晚交',
      },
      {
        key: 'submitTime',
        name: '提交时间',
      },
    ];
    const qaArr = [];
    // 构造保存写文件名前缀
    const prefix = type;
    const workFileName = prefix + '_工作日志_' + new Date().toLocaleString();
    for (const log of dailyLogData) {
      if (log.workMatters.length !== 0) {
        // 补全每条日报数据用户部门信息
        let logUserInfo = await this.ctx.model.Users.find({
          userid: log.userid,
          isDelete: false,
        }).exec();
        if (logUserInfo.length === 0) {
          continue;
        }
        logUserInfo = logUserInfo[0];
        // 获取用户所属部门详细信息
        let logUserDepartment = await this.ctx.model.Departments.find({
          id: {
            $in: logUserInfo.department,
          },
        }).exec();
        logUserDepartment = logUserDepartment[0];
        // 将每条日报数据工作事项转为二维数组
        for (const workMatters of log.workMatters) {
          workArr.push({
            userid: log.userid,
            name: logUserInfo.name ? logUserInfo.name : '',
            department: logUserDepartment.name
              ? logUserDepartment.name
              : '',
            date: log.date,
            matter: workMatters.matter ? workMatters.matter : '',
            content: workMatters.content ? workMatters.content : '',
            coworkers: workMatters.coworkers ? workMatters.coworkers : '',
            partners: workMatters.partners ? workMatters.partners : '',
            status: workMatters.status ? workMatters.status : '',
            time: workMatters.startTime2 ? workMatters.startTime + ' - ' + workMatters.endTime + ' | ' + workMatters.startTime2 + ' - ' + workMatters.endTime2 : workMatters.startTime + ' - ' + workMatters.endTime,
            plan: workMatters.plan ? workMatters.plan : '',
            suggestion: workMatters.suggestion ? workMatters.suggestion : '',
            isLate: log.isLate ? '是' : '否',
            lateReason: log.isLate ? log.lateReason : '',
            submitTime: moment(log.createTime).format('YYYY-MM-DD HH:MM'),
            info: Number(moment(log.createTime).format('YYYYMMDD')) === log.date + 1 && log.createTime.getHours() >= 14 || Number(moment(log.createTime).format('YYYYMMDD')) > log.date + 1 ? '是' : '否',
          });
        }
      }
    }
    const workJson = {};
    for (const item of workArr) {
      if (workJson[item.userid] === undefined) {
        workJson[item.userid] = [];
      }
      workJson[item.userid].push(item);
    }

    const qaJson = {};
    for (const item of qaArr) {
      if (qaJson[item.userid] === undefined) {
        qaJson[item.userid] = [];
      }
      qaJson[item.userid].push(item);
    }
    if (Object.keys(qaJson).length === 0 && Object.keys(workJson).length === 0) {
      return false;
    }
    try {
      if (Object.keys(workJson).length > 0) {
        this.jsonToXlsx(workJson, workSchema, `./${workFileName}.xlsx`);
        const workMediaUploadRes = await this.service.tool.uploadMedia(
          `./${workFileName}.xlsx`
        );
        const workMediaId = workMediaUploadRes.media_id;
        await this.service.tool.sendMsg(workMediaId, userid);
      }
      return true;
    } catch (e) {
      console.error(e.toString());
      return false;
    }
  }

  // 写入xlsx文件
  async jsonToXlsx(data, schema, dstPath) {
    const wb = XLSX.utils.book_new();
    Object.keys(data).map(userid => {
      const ws_data = [schema.map(item => item.name)];
      for (const item of data[userid]) {
        ws_data.push(schema.map(schemaItem => item[schemaItem.key]));
      }
      const ws = XLSX.utils.aoa_to_sheet(ws_data);
      XLSX.utils.book_append_sheet(wb, ws, `${data[userid][0].department}-${data[userid][0].name}`);
      return userid;
    });
    XLSX.writeFile(wb, dstPath);
    return dstPath;
  }
}
module.exports = DailyService;
