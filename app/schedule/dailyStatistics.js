'use strict';

const moment = require('moment');

module.exports = {
  schedule: {
    cron: '0 0 10 * * *', // 每天10点统计日报提交情况
    type: 'worker',
    cronOptions: {
      tz: 'Asia/Shanghai',
    },
  },
  async task(ctx) {
    const date = moment().subtract(1, 'days').format('YYYYMMDD');
    const result = await ctx.service.daily.statistic(date);
    const uncommitList = result.statisticsList.map(item => (item.status === 'uncommit' ? item.name : false)).filter(item => item !== false);
    const msg = `${moment().subtract(1, 'days').format('YYYY-MM-DD')}日报提交情况:\n共${result.commit + result.uncommit}人,\n${result.commit}人已提交,\n${result.uncommit}人未提交,\n未提交人员名单:\n${uncommitList.join(
      '、'
    )}`;
    console.log(msg);
    const res = await ctx.model.Users.find({
      identity: 'root',
    }, 'userid');
    const systemAdminList = res.map(item => item.userid);
    await ctx.service.tool.sendTextMsg(msg, systemAdminList.join(','));
  },
};
