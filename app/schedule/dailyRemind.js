'use strict';

const moment = require('moment');

module.exports = {
  schedule: {
    cron: '0 0 9 * * *', // 每天9点给未交用户发送未交提醒
    type: 'worker',
    cronOptions: {
      tz: 'Asia/Shanghai',
    },
  },
  async task(ctx) {
    const date = moment().subtract(1, 'days').format('YYYYMMDD');
    console.log(date);
    const res = ctx.model.DailyLogs.find({
      date,
      isDelete: false,
      workMatters: { $elemMatch: { $ne: null } },
    }, 'userid');
    const useridList = res.map(item => item.userid);
    const msg = `[举手] 你 ${moment().subtract(1, 'days').format('YYYY-MM-DD')} 的日报还未提交哦！`;
    console.log(msg);
    await ctx.service.tool.sendTextMsg(msg, useridList.join(','));
  },
};
