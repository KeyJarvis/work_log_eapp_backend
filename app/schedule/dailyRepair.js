'use strict';

const moment = require('moment');

module.exports = {
  schedule: {
    cron: '0 0 14 * * *', // 每天14点统计日日报补交情况
    type: 'worker',
    cronOptions: {
      tz: 'Asia/Shanghai',
    },
  },
  async task(ctx) {
    const date = moment().subtract(1, 'days').format('YYYYMMDD');
    const result = await ctx.model.DailyLogs.find({
      date,
      isLate: true,
      workMatters: { $elemMatch: { $ne: null } },
    }).populate('name', 'name');
    const repairList = result.map(item => {
      return item.name.name;
    });
    const msg = `${moment().subtract(1, 'days').format('YYYY-MM-DD')}日报补交情况:\n共${repairList.length}人补交\n${repairList.length !== 0 ? '补交人员名单:' + repairList.join('、') : ',无补交数据'}`;
    console.log(msg);
    const res = await ctx.model.Users.find({
      identity: 'root',
    }, 'userid');
    const systemAdminList = res.map(item => item.userid);
    await ctx.service.tool.sendTextMsg(msg, systemAdminList.join(','));
  },
};
