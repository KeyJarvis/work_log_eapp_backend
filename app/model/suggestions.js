'use strict';

module.exports = app => {
  const mongoose = app.mongoose;
  const Schema = mongoose.Schema;
  const SuggestionsSchema = new Schema({
    name: {
      type: String,
    },
    date: {
      type: Number,
    },
    suggestion: {
      type: String,
    },
    createTime: {
      type: Number,
      default: new Date(),
    },
    updateTime: {
      type: Date,
      default: '',
    },
    deleteTime: {
      type: Date,
      default: '',
    },
    isDelete: {
      type: Boolean,
      default: false,
    },
  });
  return mongoose.model('Suggestions', SuggestionsSchema, 'suggestions');
};
