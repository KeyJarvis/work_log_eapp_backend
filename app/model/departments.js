'use strict';

module.exports = app => {
  const mongoose = app.mongoose;
  const Schema = mongoose.Schema;
  const DepartmentsSchema = new Schema({
    id: {
      type: Number,
    },
    name: {
      type: String,
    },
    parentid: {
      type: Number,
    },
    createTime: {
      type: Date,
      default: Date.now(),
    },
    updateTime: {
      type: Date,
    },
    deleteTime: {
      type: Date,
    },
    isDelete: {
      type: Boolean,
      default: false,
    },
  });
  return mongoose.model('Departments', DepartmentsSchema, 'departments');
};
