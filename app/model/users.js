'use strict';

module.exports = app => {
  const mongoose = app.mongoose;
  const Schema = mongoose.Schema;
  const UsersSchema = new Schema({
    name: {
      type: String,
    },
    avatar: {
      type: String,
    },
    userid: {
      type: String,
    },
    department: {
      type: Array,
    },
    identity: {
      type: String,
      default: 'ordinary',
    },
    identityTime: {
      type: String,
      default: '',
    },
    createTime: {
      type: Number,
      default: new Date(),
    },
    updateTime: {
      type: Date,
      default: '',
    },
    deleteTime: {
      type: Date,
      default: '',
    },
    isDelete: {
      type: Boolean,
      default: false,
    },
  });
  return mongoose.model('Users', UsersSchema, 'users');
};
