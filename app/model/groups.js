'use strict';

module.exports = app => {
  const mongoose = app.mongoose;
  const Schema = mongoose.Schema;
  const GroupsSchema = new Schema({
    name: {
      type: String,
    },
    members: {
      type: Array,
    },
    administrators: {
      type: Array,
    },
    createTime: {
      type: Date,
      default: Date.now(),
    },
    updateTime: {
      type: Date,
    },
    deleteTime: {
      type: Date,
    },
    isDelete: {
      type: Boolean,
    },
  });
  return mongoose.model('Groups', GroupsSchema, 'groups');
};
