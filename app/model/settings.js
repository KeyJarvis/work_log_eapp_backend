'use strict';

module.exports = app => {
  const mongoose = app.mongoose;
  const Schema = mongoose.Schema;
  const SettingsSchema = new Schema({
    name: {
      type: String,
      default: '白名单',
    },
    whiteList: {
      type: Array,
      default: [],
    },
    createTime: {
      type: Date,
      default: new Date(),
    },
    updateTime: {
      type: Date,
      default: '',
    },
  });
  return mongoose.model('Settings', SettingsSchema, 'settings');
};
