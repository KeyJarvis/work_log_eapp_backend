'use strict';

module.exports = app => {
  const mongoose = app.mongoose;
  const Schema = mongoose.Schema;
  const DailyLogsSchema = new Schema({
    name: {
      type: Schema.Types.ObjectId,
      ref: 'Users',
    },
    userid: {
      type: String,
      default: '',
    },
    date: {
      type: Number,
      default: '',
    },
    isLate: {
      type: Boolean,
      default: false,
    },
    lateReason: {
      type: String,
      default: '',
    },
    workMatters: [
      {
        matter: {
          type: String,
        },
        content: {
          type: String,
        },
        coworkers: {
          type: Array,
        },
        partners: {
          type: String,
        },
        status: {
          type: String,
        },
        startTime: {
          type: String,
        },
        endTime: {
          type: String,
        },
        startTime2: {
          type: String,
        },
        endTime2: {
          type: String,
        },
        plan: {
          type: String,
        },
        suggestion: {
          type: String,
        },
      },
    ],
    createTime: {
      type: Date,
      default: Date.now(),
    },
    updateTime: {
      type: Date,
      default: '',
    },
    deleteTime: {
      type: Date,
      default: '',
    },
    isDelete: {
      type: Boolean,
      default: false,
    },
  });
  return mongoose.model('DailyLogs', DailyLogsSchema, 'dailyLogs');
};
