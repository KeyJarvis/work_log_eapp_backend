'use strict';

/**
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
  const { router, controller } = app;

  // 我的日报
  router.post('/daily/create', app.jwt, controller.daily.create); // 创建日报
  router.get('/daily/get', app.jwt, controller.daily.get); // 根据日期获取日报
  router.put('/daily/update', app.jwt, controller.daily.update); // 更新日报
  router.get('/daily/statistic', app.jwt, controller.daily.statistic); // 获取日报统计数据
  router.get('/daily/lack', app.jwt, controller.daily.lack); // 获取未交日期
  router.get('/daily/range', app.jwt, controller.daily.range); // 获取导出范围
  router.post('/daily/export', app.jwt, controller.daily.export); // 导出日报

  // 系统设置
  router.post('/system/createAdmin', app.jwt, controller.system.createAdmin); // 添加管理员
  router.get('/system/deleteAdmin', app.jwt, controller.system.deleteAdmin); // 删除管理员
  router.get('/system/adminList', app.jwt, controller.system.adminList); // 获取管理员列表
  router.post('/system/createWhite', app.jwt, controller.system.createWhite); // 添加白名单
  router.get('/system/deleteWhite', app.jwt, controller.system.deleteWhite); // 删除白名单
  router.get('/system/whiteList', app.jwt, controller.system.whiteList); // 获取白名单列表

  // tool工具类
  router.get('/login', controller.tool.login); // 钉钉免登(返回用户信息和token)
  router.get('/jsapi_sign', controller.tool.sign); // 计算签名信息返回签名
  router.get('/tool/users', app.jwt, controller.tool.getAllUserInfo); // 获取所有用户信息
  router.get('/tool/departments', app.jwt, controller.tool.getAllDepartments); // 获取所有部门信息
  router.get('/tool/update', app.jwt, controller.tool.updateData); // 更新用户和部门信息
};
