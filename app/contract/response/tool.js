'use strict';

module.exports = {
  // 登录
  loginResponse: {
    status: { type: 'number', required: true, example: 200 },
    msg: { type: 'string', required: true, example: '钉钉免登成功' },
    data: { type: 'array', required: true, itemType: 'string', example: { userInfo: { name: '', userid: '', avatar: '' } } },
  },
  // 计算签名信息返回签名
  signResponse: {
    status: { type: 'number', required: true, example: 200 },
    msg: { type: 'string', required: true, example: '获取签名成功' },
    data: { type: 'array', required: true, itemType: 'string', example: { agentId: '', corpId: '', timeStamp: '', nonceStr: '', signature: '' } },
  },

  // 获取所有用户信息
  getAllUserInfo: {
    status: { type: 'number', required: true, example: 200 },
    msg: { type: 'string', required: true, example: '获取所有用户信息成功' },
  },
  // 获取所有部门信息
  getAllDepartments: {
    status: { type: 'number', required: true, example: 200 },
    msg: { type: 'string', required: true, example: '获取所有部门信息成功' },
  },
  // 更新用户和部门信息
  updateData: {
    status: { type: 'number', required: true, example: 200 },
    msg: { type: 'string', required: true, example: '更新用户和部门信息成功' },
  },
};
