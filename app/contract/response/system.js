'use strict';

module.exports = {
  // 创建管理员
  createAdmin: {
    status: { type: 'number', required: true, example: 200 },
    msg: { type: 'string', required: true, example: '添加管理员成功' },
  },
  // 删除管理员
  deleteAdmin: {
    status: { type: 'number', required: true, example: 200 },
    msg: { type: 'string', required: true, example: '删除管理员成功' },
  },
  // 获取管理员列表
  adminList: {
    status: { type: 'number', required: true, example: 200 },
    msg: { type: 'string', required: true, example: '获取管理员列表成功' },
    data: { type: 'array', required: true, itemType: 'string', example: [{ name: '陈学义', userid: '1304455662499793140', department: ['重庆仙桃教育科技有限公司'], identity: 'root', identityTime: '2020-05-14 16:05' }] },
  },
  // 创建白名单
  createWhite: {
    status: { type: 'number', required: true, example: 200 },
    msg: { type: 'string', required: true, example: '添加白名单人员成功' },
  },
  // 删除白名单
  deleteWhite: {
    status: { type: 'number', required: true, example: 200 },
    msg: { type: 'string', required: true, example: '删除白名单人员成功' },
  },
  // 获取白名单列表
  whiteList: {
    status: { type: 'number', required: true, example: 200 },
    msg: { type: 'string', required: true, example: '获取白名单列表成功' },
    data: { type: 'array', required: true, itemType: 'string', example: [{ name: '陈学义', userid: '1304455662499793140', department: '重庆仙桃教育科技有限公司' }] },
  },
};
