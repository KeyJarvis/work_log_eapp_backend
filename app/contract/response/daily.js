'use strict';

module.exports = {
  // 基本返回
  baseResponse: {
    status: { type: 'number', required: true, example: 200 },
    msg: { type: 'string', required: true, example: 'success' },
  },
  // 创建日报
  createResponse: {
    status: { type: 'number', required: true, example: 200 },
    msg: { type: 'string', required: true, example: '创建日报成功' },
    data: { type: 'array', required: true, itemType: 'string', example: { id: '' } },
  },
  // 拉取日报
  getResponse: {
    status: { type: 'number', required: true, example: 200 },
    msg: { type: 'string', required: true, example: '获取日报数据成功' },
    data: { type: 'array', required: true, itemType: 'dailyLog' },
  },
  // 更新日报
  updateResponse: {
    status: { type: 'number', required: true, example: 200 },
    msg: { type: 'string', required: true, example: '更新日报成功' },
  },
  // 日报统计数据
  statisticResponse: {
    status: { type: 'number', required: true, example: 200 },
    msg: { type: 'string', required: true, example: '获取日报统计数据成功' },
    data: { type: 'array', required: true, itemType: 'string', example: { commit: 1, uncommit: 54, statisticsList: [{ name: '刘世望', status: 'commit' }] } },
  },
  // 获取未交日期
  lackResponse: {
    status: { type: 'number', required: true, example: 200 },
    msg: { type: 'string', required: true, example: '获取未交日期成功' },
    data: { type: 'array', required: true, itemType: 'string', example: { alreadyDateList: ['20200509', '20200510'] } },
  },
  // 获取同事列表
  colleagueResponse: {
    status: { type: 'number', required: true, example: 200 },
    msg: { type: 'string', required: true, example: '获取同事列表成功' },
    data: { type: 'array', required: true, itemType: 'string', example: { colleagueList: [{ value: '', label: '', children: [{ value: '', label: '' }] }] } },
  },
  // 导出日报
  exportResponse: {
    status: { type: 'number', required: true, example: 200 },
    msg: { type: 'string', required: true, example: '导出日报成功' },
  },
};
