'use strict';

module.exports = {
  // 创建日报_body
  postCreateRequest: {
    date: { type: 'number', description: '提交日期', example: 20200508 },
    workMatters: { type: 'array', itemType: 'workMatters', description: '事项内容' },
  },
  // 更新日报_body
  postUpdateRequest: {
    id: { type: 'string', description: '提交日期', example: '5ec3cdc2cb8bce7264c36952' },
    workMatters: { type: 'array', itemType: 'workMatters', description: '事项内容' },
  },
  // 导出日报_body
  postExportRequest: {
    type: { type: 'number', description: '人员范围', example: 0, required: true },
    time: { type: 'number', description: '时间类型', example: 1 },
    startTime: { type: 'number', description: '开始时间', example: 20200508 },
    endTime: { type: 'number', description: '结束时间', example: 20200512 },
  },
};
