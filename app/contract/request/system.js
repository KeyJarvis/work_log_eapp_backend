'use strict';

module.exports = {
  // 创建管理员_body
  postCreateAdmin: {
    identity: { type: 'string', description: '管理员权限', example: 'root' },
    name: { type: 'array', description: '管理员名称', itemType: 'string', example: ['刘世望', '杨耀东'] },
  },
  // 创建白名单_body
  postCreateWhite: {
    userid: { type: 'array', description: '管理员权限', itemType: 'string', example: ['1304455662499793140', '1304455662499793123'] },
  },
};
