'use strict';

module.exports = {
  // 创建日报发送事项
  workMatters: {
    matter: {
      type: 'string',
      description: '工作事项',
      example: '',
    },
    content: {
      type: 'string',
      description: '工作内容',
      example: '',
    },
    coworkers: {
      type: 'array',
      itemType: 'string',
      description: '参与同事',
      example: [],
    },
    partners: {
      type: 'string',
      description: '参与伙伴',
      example: '',
    },
    status: {
      type: 'string',
      description: '状态效果',
      example: '',
    },
    startTime: {
      type: 'string',
      description: '开始时间',
      example: '',
    },
    endTime: {
      type: 'string',
      description: '结束时间',
      example: '',
    },
    startTime2: {
      type: 'string',
      description: '开始时间2',
      example: '',
    },
    endTime2: {
      type: 'string',
      description: '结束时间2',
      example: '',
    },
    plan: {
      type: 'string',
      description: '后续计划',
      example: '',
    },
    suggestion: {
      type: 'string',
      description: '问题建议',
      example: '',
    },
  },
  // 拉取日报返回日报数据
  dailyLog: {
    userid: {
      type: 'string',
      description: '用户id',
      example: '',
    },
    date: {
      type: 'number',
      description: '日报日期',
      example: 20200508,
    },
    isLate: {
      type: 'boolean',
      description: '是否补交',
      example: false,
    },
    lateReason: {
      type: 'string',
      description: '补交原因',
      example: '',
    },
    createTime: {
      type: 'date',
      description: '创建时间',
      example: '',
    },
    updateTime: {
      type: 'date',
      description: '更新时间',
      example: '',
    },
    deleteTime: {
      type: 'date',
      description: '删除时间',
      example: '',
    },
    isDelete: {
      type: 'boolean',
      description: '删除时间',
      example: '',
    },
    _id: {
      type: 'string',
      description: '数据id',
      example: '',
    },
    workMatters: {
      type: 'array',
      description: '工作事项',
      itemType: 'workMatters',
    },
    name: {
      type: 'string',
      description: '姓名id',
      example: '',
    },
  },
};
