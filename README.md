# work_log_eapp_backend

钉钉微应用（日报）

### Overview

基于 [egg.js](https://eggjs.org/zh-cn/intro/quickstart.html) 实现的钉钉微应用后端

### Project Download

```bash
git clone https://gitlab.com/KeyJarvis/work_log_eapp_backend.git
```

### Project operation

```bash
$ npm install
$ npm run dev
$ open http://localhost:7001/
```

### Others

- Use `npm run lint` to check code style.
- Use `npm test` to run unit test.
- Use `npm run autod` to auto detect dependencies upgrade, see [autod](https://www.npmjs.com/package/autod) for more detail.
